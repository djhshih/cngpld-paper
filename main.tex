\documentclass{bioinfo}
\copyrightyear{20XX} \pubyear{20XX}

\access{Advance Access Publication Date: Day Month Year}
\appnotes{Original Paper}

\begin{document}
\firstpage{1}

\subtitle{Genome analysis}

\title[CNGPLD]{CNGPLD: Case-control copy-number analysis using Gaussian process latent difference}
\author[Shih \textit{et~al}.]{%
David J. H. Shih\,$^{\text{\sfb 1,2,7}}$,
Ruoxing Li\,$^{\text{\sfb 7}}$,
Peter M\"uller\,$^{\text{\sfb 3,4}}$,
W.\ Jim Zheng\,$^{\text{\sfb 7}}$,
Kim-Anh Do\,$^{\text{\sfb 4}}$,
Shiaw-Yih Lin\,$^{\text{\sfb 2}}$,
and Scott L. Carter\,$^{\text{\sfb 1,5,6}*}$}
\address{%
$^{\text{\sf 1}}$Broad Institute of Massachusetts Institute of Technology and Harvard, Cambridge, MA, 02142, USA. 
$^{\text{\sf 2}}$Department of Systems Biology, University of Texas MD Anderson Cancer Center, Houston, TX, 77030, USA. 
$^{\text{\sf 3}}$Department of Mathematics and Department of Statistics \& Data Science, University of Texas Austin Houston, TX, 78712, USA. 
$^{\text{\sf 4}}$Department of Biostatistics, University of Texas MD Anderson Cancer Center, Houston, TX, 77030, USA. 
$^{\text{\sf 5}}$Department of Biostatistics, Harvard T.H. Chan School of Public Health, Boston, MA, 02115, USA. 
$^{\text{\sf 6}}$Department of Data Sciences, Dana-Farber Cancer Institute, Boston, MA, 02115, USA.
$^{\text{\sf 7}}$School of Biomedical Informatics, University of Texas Health Science Center at Houston, Houston, TX, 77030, USA.}

\corresp{$^\ast$To whom correspondence should be addressed.}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\abstract{\textbf{Motivation:}
Cross-sectional analyses of primary cancer genomes have identified regions of 
recurrent somatic copy-number alteration, many of which result from positive selection 
during cancer formation and contain driver genes.   
However, no effective approach exists for identifying genomic loci under 
significantly different degrees of selection in cancers 
of different subtypes, anatomic sites, or disease stages. \\
\textbf{Results:} CNGPLD is a new tool for performing case-control somatic copy-number analysis
that facilitates the discovery of differentially amplified or deleted copy-number aberrations in
a case group of cancer compared to a control group of cancer.
This tool uses a Gaussian process statistical framework in order to account for 
the covariance structure of copy-number data along genomic coordinates 
and to control the false discovery rate at the region level.\\
\textbf{Availability:} CNGPLD is freely available at
\href{https://bitbucket.org/djhshih/cngpld}https://bitbucket.org/djhshih/cngpld as an R package.\\
\textbf{Contact:} \href{scarter@broadinstitute.org}{scarter@broadinstitute.org}\\
\textbf{Supplementary information:} Supplementary data are available at \textit{Bioinformatics}
online.}

\maketitle

\enlargethispage{25pt}

\section{Introduction}

Somatic copy-number aberrations (CNAs) drive cancer evolution, and the
identification of recurrently amplified or deleted regions in
cross-sectional studies of primary cancers has implicated genes
as proto-oncogenes or tumor suppressors in cancer formation.
In recent years, case-control genomic studies are becoming more common,
especially for studying cancer recurrence and metastasis. For example,
we have previously discovered novel drivers of metastatic progression
by comparing the copy-number profiles of brain metastases (case) 
vs. primary disease (control) \citep{shih20}.
In these case-control analyses, the objective is to identify genomic
regions that are significantly differentially amplified or deleted in cases vs.\ controls,
given a user-specified genome-wide false-discovery rate (FDR) threshold.
The cases and controls may involve any comparisons of interesting,
such as comparing cancers of different subtypes, anatomic sites, or disease stages.

This comparative copy-number analysis raises a multitude of technical challenges.
Recurrent somatic aberrations in the two cohorts often have limited
overlap due to the intrinsic uncertainty in delimiting the boundaries of a
recurrent copy-number aberration, which complicates the use of simple
approaches such as set subtraction of recurrently disrupted regions
that are identified in cases vs.\ controls.
Additionally, copy-number states of nearby loci are highly correlated, 
which can cause the failure of standard adjustment methods to control the false
discovery rate following the application of marginal tests such as
the Fisher's exact test \citep{korthauer18}.

To overcome these challenges, we have developed a new tool for 
case-control somatic copy-number analysis that uses a Gaussian process prior 
to capture the covariance structure of copy-number data and to identify 
differentially amplified or deleted regions while maintaining control of
FDR at the region level.

\vspace*{-15pt}

\begin{methods}
\section{Methods}

\subsection{Copy-number data}

Copy-number profiles for samples $1 < i < N$ across loci $1 < j < J$ typically consist
of log relative copy-number estimates $r_{ij}$ and positions $x_j$.
In order to denoise the copy-number profiles, segmentation is often performed
such that segments of the profiles will share the same copy-number estimate.
That is, for a copy-number segment across loci $[s, t]$ in sample $i$,
$r_{ij} = r_{is}$ for all $j \in [s, t]$.
The segmented profile of sample $i$ for segment $k$ is often encoded as $(s_k, t_k, r_{ik})$, 
where $s_k$ is the segment start position, $t_k$ the end position, and 
$r_{ik}$ the copy-number estimate of the segment.
This segment-wise encoding $(s_k, t_k, r_{ik})$ can be converted to
the marker-wise encoding by instantiating $(x_j, r_{ik})$ for all $x_j \in [s_k, t_k]$.

Given the segmented log relative copy-number estimates $r_{ij}$, we compute the group 
summary amplification and deletion profiles by summing across samples:
$$
\begin{aligned}
	y^{\text{amp}}_{j} &= \frac{1}{N} \sum_{i} \exp(r_{ij}) \, \mathrm{I}(r_{ij} > \rho) \\
	y^{\text{del}}_{j} &= \frac{1}{N} \sum_{i} \exp(-r_{ij}) \, \mathrm{I}(r_{ij} < -\rho)
\end{aligned}
$$
where $\rho$ is the copy-number threshold.
The summary scores $y^{\text{amp}}_{j}$ and $y^{\text{del}}_{j}$ 
capture both the amplitude and frequency of recurrent CNAs that occur 
across samples.
Since our summary scores use $\exp(r_{ij})$,
they are linear with respect to the copy-number changes,
similar to G-scores computed by GISTIC2 \citep{mermel11}.
All downstream procedures will analyze summary amplification and 
deletion profiles separately.
Additionally, although individual copy-number profiles are discrete, 
the group summary profiles marginalized across samples tends 
toward a continuous distribution as the sample size increases. 

If we were only interested in the amplification and deletion frequencies,
we would instead use
$$
\begin{aligned}
	v^{\text{amp}}_{j} &= \frac{1}{N} \sum_{i} \mathrm{I}(r_{ij} > \rho) \\
	v^{\text{del}}_{j} &= \frac{1}{N} \sum_{i} \mathrm{I}(r_{ij} < -\rho).
\end{aligned}
$$

Our objective is to compare the group summary amplification (or deletion) profile
of a case group to that of a control group, in order to identify regions that are 
more frequently amplified (or deleted) in cases compared to controls.
However, two complications arise for this comparison:
\begin{enumerate}
	\item Summary scores at nearby genomic positions are positively correlated and 
	thus cannot be treated as independent.
	\item The genomic loci positions can be different between case and control, due
	to various factors including platform differences and cohort-specific
	summary profile normalization.
\end{enumerate}
To address both complications, we use a Gaussian process model to estimate the 
latent (unobserved) differences in summary profiles between the case and control 
groups in order to identify significant focal regions of differences.

\subsection{Sample matching}

We assume that the case and control groups have been appropriately
matched so as to mitigate confounding by uncontrolled covariates \citep{shih20}.
The case and control groups should be matched using covariates that are known or
likely confounding variables, which affect both the explanatory variable 
(e.g. copy-number aberrations) and the outcome variable of interest.
Failure to match the two groups adequately will likely result in false positives 
due to uncontrolled covariates.
We have had success with coarsened exacting matching (as implemented in the 
\texttt{MatchIt} R package), which provides matching weights that ensure exact 
balance \citep{iacus12,ho11}.
Another common method is propensity score matching.

\subsection{Data preprocessing}

In order to improve the sensitivity of identifying focal (rather than broad) 
CNAs, we center the segmentation profile $r_{ij}$ of each 
sample by the mean log copy-number value across segments on each 
chromosome arm, weighted by the segment sizes. 
For each sample, we derive the log copy-number profiles from the segmentation 
profiles at all observed breakpoints across all samples, and we then apply running 
median smoothing.
Thereafter, we summarize the log copy-number profiles into summary 
amplification and deletion profiles as described above. 
We split these summary profiles by chromosome arms to improve computational speed,
because the subsequent Gaussian process model fitting has time complexity $O(J^3)$ 
where $J$ is the number of loci. In the same vein of keeping $J$ small, 
we also coarsen the summary scores by rounding to the second decimal place 
and merge adjacent data points that have the same scores.

Internally, we work with genomic coordinates $x_j$ in units of Mbp, 
so the hyperparameter $\lambda^2$  is in Mbp$^2$.
Keeping $x_j$ relative small helps avoid numeric instability.
Values of $y_j$ are already small enough, so we do not apply additional
transformation.

For each chromosome arm, we combine the case and control summary profiles 
together into a merged summary profile $y_j$ with associated positions 
$x_j$ for $j \in \{1 ... J \}$,
while introducing group membership variable
$g_j \in \left\{-\frac{1}{2}, +\frac{1}{2} \right\}$
to distinguish data points from controls and cases, respectively.
(See Supplementary Information for the statistical justification for this choice of 
membership values.).

\subsection{Statistical model}

Given genomic positions $\mathbf{x} \in \mathbb{R}^J$ across $J$ loci,
as well as the merged summary amplification (or deletion) profile 
$\mathbf{y} \in \mathbb{R}^J$,  we fit the following model in order 
to estimate the latent difference function $f$ of case vs.\ control profiles:
$$
\begin{aligned}
	y_j &\sim \text{Normal} \left( \mu + g_j f(x_j), \sigma^2 \right), &\quad
	f &\sim \text{GaussianProcess} \left( 0, k \right) \\
	\mu &\sim \text{Normal} \left( 0, \tau^2 \right), &\quad
	\sigma^2 &\sim \text{InvGamma} \left( \alpha, \beta \right)
\end{aligned}
$$
where $j$ indexes genomic locus, and $k(\cdot)$ is the squared exponential covariance function
defined as
$$
\mathit{k}(x_j, x_{j'}) = \nu^2 \exp\left( -\frac{1}{2 \lambda^2} \left( x_j - x_{j'} \right)^2 \right).
$$
We recognize that the retrospective sampling design could introduce an 
overall bias such that $\mu \neq 0$.
Since $\mu$ is not the main inference target,
we apply an independent normal prior on it.
Moreover, separate instances of this model are used to 
independently analyze each event type (amplification or deletion), 
as well as each chromosome arm.

The model parameters $(f, \mu, \sigma^2)$ are estimated using the 
iterated conditional mode (coordinate ascent) approach, 
for which we have derived closed-form update equations 
in order to achieve computational efficiency. 
Define $\mathbf{1}$ as a column vector of ones,
$\mathbf{G} = \text{diag}(\mathbf{g})$, and
$\mathbf{f}[j] = \mathit{f}(x_j)$.
The update equations of the model parameters are as follows:
$$
\begin{aligned}
	\hat{\mathbf{f}} &= \left( \mathbf{G}^2 + \sigma^2 \mathbf{\Sigma}^{-1} \right)^{-1} 
	\mathbf{G} (\mathbf{y} - \mathbf{\mu} \mathbf{1}) \\
	\hat{\mu} &= \frac{\tau^2}{J \tau^2 + \sigma^2} \mathbf{1}^\top 
		(\mathbf{y} - \mathbf{G} \mathbf{f}) \\
	\hat{\sigma^2} &= \frac{
		\beta + \frac{1}{2} 
		(\mathbf{y} - \mu \mathbf{1} - \mathbf{G} \mathbf{f})^2
	} { \frac{J}{2} + \alpha + 1 } .
\end{aligned}
$$

After model fitting, $f(x_j) > 0$ if locus $j$ is more frequently disrupted
in the case cohort as compared to the control cohort. In order to
perform inference on the latent difference $f$, we approximate its posterior
distribution by Laplace's method:
$$
\mathbf{f} \mid \mathbf{y} \sim \text{Normal}(\hat{\mathbf{f}}, \hat{\mathbf{V}}) ,
$$
where $\hat{\mathbf{V}}$ is given by the inverse of the Hessian of the log posterior:
$$
\begin{aligned}
	\hat{\mathbf{V}}
		&= \left( \frac{1}{\sigma^2}\mathbf{G}^2  + \mathbf{\Sigma}^{-1} \right)^{-1} .
\end{aligned}
$$

In the Supplementary Information, we provide the derivations of the update 
equations for the model parameters and the derivation of $\hat{\mathbf{V}}$;
further, we prove that $\hat{\mathbf{V}}$ is a valid covariance matrix.

\subsection{Model inference}

To identify differentially amplified (or deleted) regions, we use a
two-step procedure inspired by the method of \cite{korthauer18}
for identifying differentially methylated regions, and we control
the FDR at the region level.

The objective is to delineate all regions with positions $[l, k]$ 
such that $\bar{f}_{[l, k]} > 0$ with high probability, where
$\bar{f}_{[l, k]} \equiv \frac{1}{k - l + 1} \sum_{j=l}^k f(x_j)$ is the mean latent
difference within the region. We proceed as follows:

\begin{enumerate}
	\item Identify candidate regions by the following steps.
		\begin{enumerate}
			\item Find contiguous regions $[l, k]$ such that the posterior log odds
			$\text{log} P \left( f(x_j) > 0 \mid y \right) - \text{log} P \left( f(x_j) \le 0 \mid y \right) > 10$ for all $j \in [l, k]$.
			\item Merge adjacent contiguous regions separated by a gap of $\le 5$ loci together in order to form the candidate regions.
			\item Calculate the posterior probability $P \left( \bar{f}_{[l, k]} > 0 \mid y \right)$ for each candidate region.
		\end{enumerate}
	\item Control for the Bayesian false discovery rate using the posterior 
	probabilities of the candidate regions \citep{muller06}.
\end{enumerate}

\subsection{Hyperparameter estimation}

For case-control copy-number analysis, we have pretuned the hyperparameters after
having applied the model to copy-number data from diverse platforms
(e.g. Affymetrics SNP6 microarray, exome sequencing with Agilent or Illumina hybrid capture,
targeted sequencing). Giving our data pre-processing and scaling, our model is insensitive to the setting of most
of the hyperparameters. The most critical  is the $\lambda^2$ hyperparameter in the
covariance function, which governs how quickly the influence that a locus has on nearby loci decays as
a function of distance in the estimation of the latent difference $f$.
Larger $\lambda^2$ would help the model to identify larger differential 
regions at the expense of smaller regions.
Because we model genomic coordinates rather than using integer indices,
our model is aware of the distances between genomic loci.
Additionally, after the coarsening of summary scores and merging of adjacent 
markers, the input to the Gaussian process model typically consists of 
$J \in [50, 500]$ data points per chromosome arm, so our model does not 
require very high resolution copy-number data.
For these reasons, the choice of $\lambda^2$ is largely platform-independent.
Rather, the choice of $\lambda^2$ depends on the investigator's preference
 to prioritize more focal or more broad recurrent copy-number events.

In general, with appropriate scaling of data points $(x_j, y_j)$, most hyperparameters 
can be set to reasonable default values for other applications of the 
Gaussian process latent difference model.
However, reasonable values of length-scale hyperparameter $\lambda^2$ and 
amplitude hyperparameter $\nu^2$ of the covariance function may depend on the
signal-to-noise ratio of the data. In Supplementary Information,
we describe methods for estimating these hyperparameters based on the 
empirical Bayes approach, whereby we maximize the marginal likelihood of 
the data $P \left(\mathbf{y} \mid \mathbf{x} \right)$.

\subsection{Implementation}

Software implementations are available as R packages.
The general Gaussian process latent difference model is implemented in the 
\texttt{gpldiff} package.
The algorithm that is written specifically to preprocess and analyze case-control
copy-number data is implemented in the \texttt{cngpld} package,
which supports taking segmentation files or G-scores as inputs.

To avoid numerical difficulties, we follow recommendations by \cite{rasmussen05}
whenever we compute matrices of the form
$
(\mathbf{K}^{-1} + \mathbf{D})^{-1}
$
where $\mathbf{D}$ is a diagonal matrix and $\mathbf{K}$ is a symmetric positive semidefinite matrix. 
Namely, we compute
$$
(\mathbf{K}^{-1} + \mathbf{D})^{-1} 
= \mathbf{K} - \mathbf{K} \mathbf{D}^{\frac{1}{2}}
\mathbf{B}^{-1} \mathbf{D}^{\frac{1}{2}} \mathbf{K}
$$
where $\mathbf{B} = \mathbf{I} + \mathbf{D}^{\frac{1}{2}} \mathbf{K} \mathbf{D}^{\frac{1}{2}}$
is symmetric positive definite.

\subsection{Performance evaluation}

We generated synthetic benchmark datasets starting with the segmented 
copy-number data from the lung adenocarcinoma (n = 859) and lung squamous 
cell carcinoma (n = 681) cohorts in the PanCanAtlas \citep{weinstein13}.
We first permuted the sample labels and assigned 859 samples to cases and 681
samples to controls. We then generated recurrent focal copy-number events
at random genomic locations with variable relative log ratios and variable
event frequencies. After filtering recurrent events that overlap with one another,
as well as with the centromeres, we had our ground-truth of target 
differential amplifications and deletion regions.
From these target events, we generated CNA segments with variable log ratios 
and expanded sizes (by up to 50\% downstream or upstream) that must contain 
the target regions, and inserted the CNA segments into the segmented 
profiles of the case samples.

To change the difficulty of the synthetic benchmark, we varied the
signal-to-noise and the frequency of target events.
We define signal-to-noise (SNR) as the ratio of the log relative copy-number estimate
of the target event to the standard deviation of log relative copy-number estimates
across all samples in the cohort. The event frequency is simply the
fraction of case samples that habour the target event.

To ensure that the spike-in target events and CNAs segments 
reflect real copy-number events as much as possible,
we varied log ratios and frequencies of target events within a dataset by 
drawing these parameters from the log normal distribution and the 
beta distribution, respectively, so that
each synthetic benchmark dataset contains heterogeneous target events,
similar to real datasets (which do not have comprehensive ground-truth available).
Similarly, the sizes and log ratios of CNA segments were varied among samples,
in order to ensure that the spike-in CNA segments are also heterogeneous. 

Using the synthetic benchmark, we evaluated the performances of competing 
methods using precision-recall curves.
Given a set of ground-truth regions, and a set of called regions, we define
a match as a ground-truth region that overlaps with a called regions
with a Jaccard index $> 0.2$.
Accordingly, we declare called regions that match with any ground-truth
as true positives, all unmatched called regions as false positives,
and all unmatched ground-truth regions as false negatives.
We calculated the area under the precision-recall curve using 
the method of \cite{davis06}.

\subsection{General problem}

We recognize that our Gaussian process latent difference model can solve a 
broader class of problems that involves comparing two curves from which 
data points are observed with both noise and missingness.
We thus describe this general problem of curve comparison in the 
Supplementary Information.

\end{methods}

\section{Results}

\subsection{Synthetic benchmark}

In order to create a realistic dataset with ground truth labels, we started with a 
real biological copy-number dataset from the PanCanAtlas \citep{weinstein13},
removed signals from it by permutation, and finally spiked in ground-truth events 
that the competing models will need to identify.

We compared our CNGPLD model against the Fisher's exact test, which is a
popular method for case-control copy-number analysis \citep{litchfield21,watkins20}.
After the same data pre-processing procedure, we computed the summary 
amplification and deletion scores for the CNGPLD method, and we computed
the frequencies of amplifications and deletions for the Fisher's exact test.
We evaluated the performances of these methods using precision-recall curves,
which is suitable for rare events \citep{davis06}, such as recurrent CNAs.
For a moderately difficult benchmark with event 
signal-to-noise (SNR) of 1.5 and frequency of 0.2,
CNGPLD outperforms the Fisher's exact test by a large margin, achieving
an area under the precision-recall curve (PR-AUC) of 0.86, compared
to 0.46 (\textbf{Fig.~\ref{fig:1}}, \textbf{Table~\ref{tab:1}}).

\begin{figure}[h]
	\vspace*{10pt}
	\begin{center}
		\includegraphics[width=0.47\textwidth]{pr_cngpld-vs-fisher}
		\caption{%
			(A) Precision-recall curves of CNGPLD vs. Fisher-exact test on a benchmark with $\text{SNR} = 1.5$ and $\text{frequency} = 0.2$.
			(B) Box plot of performance statistics at target FDR. *, Wilcoxon rank-sum test $p < 0.001$
		}\label{fig:1}
	\end{center}
	\vspace*{-10pt}
\end{figure}

When differential regions are called by each method at a FDR threshold of 10\%,
CNGPLD achieves much higher recall (sensitivity)
than Fisher's exact test (\textbf{Fig.~\ref{fig:1}B}, \textbf{Table~\ref{tab:1}}), 
while maintaining high precision.
Conversely, Fisher's exact test completely fails to control the
FDR, having achieved a precision of only 0.57,
due to the fact that it controls FDR at the locus level,
which does not translate to FDR at the region level \citep{korthauer18}.

The Jaccard index between called and ground-truth target regions for both methods
are comparable (\textbf{Table~\ref{tab:1}}).
The fractions of target regions that are covered by the called
regions are also comparable between the methods.
The sizes of the called regions relative to those of the target regions are
also similar between the methods.
These results indicate that both CNGPLD and Fisher's exact test usually identify
differential regions that completely span the ground-truth target regions,
albeit with wide margins.

\begin{table}[h]
	\processtable{Performance statistics of CNGPLD vs.\ Fisher's exact test. \label{tab:1}} {
		\begin{tabular}{@{}lll@{}}
			\toprule
			measurement & CNGPLD & Fisher \\
			\midrule
			precision & 0.88 (0.03) & 0.57 (0.11) \\
			recall & 0.87 (0.05) & 0.49 (0.08) \\
			Jaccard index & 0.54 (0.03) & 0.58 (0.06) \\
			coverage fraction of target region & 0.99 (0.01) & 0.98 (0.02) \\
			relative called region size & 1.86 (0.11) & 1.65 (0.16) \\
			PR-AUC & 0.86 (0.04) & 0.46 (0.07) \\
			\botrule
	\end{tabular}}%
	{Cells contain mean (SD) values across 100 runs. Measurements are obtained at 10\% FDR threshold, except for PR-AUC.}
\end{table}

In order to compare model performances under different benchmark difficulties,
we varied the SNR and frequency of the events.
CNGPLD consistently and strongly outperforms the Fisher's exact test across all SNRs 
(\textbf{Fig.~\ref{fig:2}A}) and all frequencies (\textbf{Fig.~\ref{fig:2}B}).
Remarkably, CNGPLD is still able to detect target events at SNR $\le 1$ (\textbf{Fig.~\ref{fig:2}A}),
which represent very difficult problems. CNGPLD can detect events with low SNR likely
because it models the spatial correlation structure and borrows information across nearby
genomic loci, whereas Fisher's exact test performs hypothesis tests independently
across loci. Moreover, CNGPLD can also detect rare events (given a sufficiently large sample size)
with much higher sensitivity than Fisher's exact test (\textbf{Fig.~\ref{fig:2}B}).
In addition to modeling spatial correlation,
CNGPLD also considers the amplitude of recurrent CNAs, as provided in the summary scores;
in contrast, Fisher's exact test only considers event frequencies.

\begin{figure}[h]
	\vspace*{10pt}
	\begin{center}
		\includegraphics[width=0.47\textwidth]{pr_snr_freq_n}
		\caption{%
			Precision-recall curves on benchmarks with varying event signal-to-noise (A),
			event frequency (B), or sample size per group (C).
		}\label{fig:2}
	\end{center}
	\vspace*{-10pt}
\end{figure}

Further, we assessed how CNGPLD performs under small sample sizes, because
copy-number estimates are naturally discrete (after modeling tumour purity,
tumour ploidy, and CNA clonality), and the summary scores that CNGPLD
use as inputs are approximately continuous only if the scores are
summed over a sufficiently large sample size. CNGPLD strongly outperforms
the Fisher's exact test across all sample sizes (\textbf{Fig.~\ref{fig:2}C}).
Notably, CNGPLD can detect recurrent CNAs with sample sizes of 10--40
per group, whereas the recalls of the Fisher's exact test are near 0 at
these sample sizes.
The higher sensitivity of CNGPLD at small samples come at the cost
of lower precision, though CNGPLD vastly outperforms Fisher's exact test
in terms of recall and PR-AUC (\textbf{Table~\ref{tab:2}}).

\begin{table}[h]
	\processtable{Precision and recall for varying sample sizes. \label{tab:2}} {%
		\begin{tabular}{@{}llllll@{}}%
			\toprule
			N & method & precision & recall & PR-AUC \\
			\midrule
			10  & CNGPLD & 0.66 (0.13) & 0.46 (0.08) & 0.39 (0.09) \\
			& Fisher & -- & 0 & 0 \\
			20  & CNGPLD & 0.70 (0.08) & 0.63 (0.08) & 0.55 (0.08) \\
			& Fisher & -- & 0 & 0 \\
			40  & CNGPLD & 0.72 (0.08) & 0.73 (0.05) & 0.67 (0.06) \\
			& Fisher & 0.99 (0.04) & 0.02 (0.04) & 0.01 (0.03) \\
			80  & CNGPLD & 0.71 (0.06) & 0.77 (0.05) & 0.73 (0.07) \\
			& Fisher & 0.89 (0.13) & 0.21 (0.08) & 0.18 (0.08) \\
			160  & CNGPLD & 0.76 (0.04) & 0.82 (0.05) & 0.80 (0.05) \\
			& Fisher & 0.90 (0.12) & 0.32 (0.08) & 0.29 (0.08) \\
			320  & CNGPLD & 0.81 (0.04) & 0.84 (0.05) & 0.83 (0.05) \\
			& Fisher & 0.87 (0.10) & 0.41 (0.08) & 0.38 (0.08) \\
			640  & CNGPLD & 0.87 (0.04) & 0.87 (0.04) & 0.86 (0.04) \\
			& Fisher & 0.72 (0.14) & 0.47 (0.07) & 0.44 (0.07) \\
			\botrule
	\end{tabular}}%
	{Cells contain mean (SD) values across 30 runs. Precision and recall are obtained at 10\% FDR threshold.
		N, sample size per group.}
\end{table}

\subsection{Sensitivity analysis}

Since the performance of Bayesian models can sometimes be dependent
on the hyperparameters,
we evaluated the sensitivity of the CNGPLD model to the hyperparameter
settings on the synthetic benchmark with $\text{SNR} = 1.5$ and
$\text{frequency} = 0.2$. CNGPLD is insensitive to the setting of
most hyperparameters, including $\nu^2$, $\alpha$, and $\tau^2$, since
its precision-recall curves are nearly identical over a wide range of settings
of these hyperparameters (\textbf{Fig.~\ref{fig:3}}).
As expected, small values of $\lambda^2$
have a modest adverse impact on the sensitivity of the model,
because it causes the influence of nearby genomic loci to decay
more rapidly as a function of distance in the squared exponential 
kernel, leading to less borrowing of information across loci.
Conversely, CNGPLD underperforms when $\beta$ is too large,
which translates to a prior on the observation error parameter
$\sigma^2$ that is skewed towards larger $\sigma^2$ (\textbf{Fig.~\ref{fig:3}}).
Observation error $\sigma^2$ competes with latent difference $f$
as explanations for variations in summary scores $\mathbf{y}$;
therefore, large $\sigma^2$ can lead to real signals in the summary
scores to be dismissed as noise.
However, the prior on $\sigma^2$ also cannot be too skewed towards 0.
Simulation analyses (based on direct sampling) using known model parameters 
show that the Gaussian process latent difference model 
often overestimates $\sigma^2$ in order to absorb the effect of
imperfect model specification, and constraining $\sigma^2$
to the true value can have a detrimental effect on
model predictions (Supplementary Information).
Taken together, CNGPLD is insensitivity to most hyperparameters,
and the default values allows CNGPLD to achieve precisions and
recalls that are vastly better than Fisher's exact test.

\begin{figure}[h]
	\vspace*{10pt}
	\begin{center}
		\includegraphics[width=0.50\textwidth]{pr_hparams}
		\caption{%
			Sensitivity analysis of CNGPLD model to hyperparameter settings.
		}\label{fig:3}
	\end{center}
	\vspace*{-10pt}
\end{figure}

\subsection{Biological study}

We illustrate the application of this tool to identify regions that are 
differentially amplified between lung adenocarcinoma (case) and lung squamous cell 
carcinoma (control). Segmented copy-number profiles for these tumour subtypes were obtained 
from PanCanAtlas \citep{weinstein13}.
These segmented profiles were analyzed by the CNGPLD model, which identified
\textit{NKX2-1} (\textit{TTF-1}) as a gene that is more frequently amplified in 
adenocarcinoma compared to squamous cell carcinoma (\textbf{Fig.~\ref{fig:4}}).
This is consistent with the common use of \textit{TTF-1} as a lineage marker for lung adenocarcinoma.
The model also identified the \textit{CCND1} locus as a region that is significantly 
more frequently amplified in squamous cell carcinoma compared to adenocarcinoma.
Taken together, CNGPLD can identify biologically important genes in case-control
copy-number analysis of different cancer subtypes.

\begin{figure}[h]
	\vspace*{10pt}
	\begin{center}
		\includegraphics[width=0.4\textwidth]{cngpld_luad-vs-lusc_opt.pdf}
		\caption{(A) Volcano plot of significant regions comparing lung adenocarcinoma (case) vs.\ lung squamous cell carcinoma (control).
			(B) Observed cohort summary scores, inferred latent difference, and log 
			posterior odds on chromosomes harbouring significant regions.
		}\label{fig:4}
	\end{center}
	\vspace*{-10pt}
\end{figure}

\subsection{Functional validation}

We have previously used CNGPLD to identify metastatic drivers by comparing a cohort of 
brain metastatic lung adenocarcinoma patients ($n = 73$) 
to a cohort of primary lung adenocarcinoma patients ($n = 503$) \citep{shih20}.
In this study, we identified three candidate genes
(\emph{MYC}, \emph{YAP1}, and \emph{MMP13}) that
were more frequently amplified in brain metastases compared to primary tumours.
We performed animal xenograft experiments in which tumour cells were engineered
to overexpress the candidate genes, and we showed that all three candidate 
drivers do indeed increase the incidence of brain metastases following 
intracardiac injections of the tumour cells \citep{shih20}.
Therefore, CNGPLD produced predictions that were later functionally validated 
\emph{in vivo}.

\section{Discussion}

We have developed a Gaussian process latent difference model for case-control copy-number studies.
These studies can involve comparing cancer subtypes,
such as lung adenocarcinoma or lung squamous cell carinoma, as we illustrated here.
This comparison is of interest because clinically, they are both categorized as non-small cell lung cancer,
even though their genomic landscapes and biological abnormalities are very distinct \citep{campbell16}.
Alternatively, investigators can also compare metastatic tumours against primary tumours,
similar to our previous study of comparing brain metastases against primary lung adenocarcinomas
\citep{shih20}.
Other examples of biological or clinical interest include comparisons of recurrent vs.\ primary tumours,
drug resistant vs.\ sensitive tumours, or pre vs.\ post treatment tumours.
Now that many cross-sectional studies of major types of primary cancers 
have been completed,
increasing numbers of genomics studies will surely involve case-control comparisons.

Conventional statistical methods such as Fisher's exact test and logistic regression
have been applied to case-control comparisons; however, these methods test each loci
independently. This flaw severely limits the sensitivity of hypothesis testing, as 
our results have shown: the Fisher's exact test consistently have much lower
recall in case-control copy-number comparisons.
In contrast, our CNGPLD method takes advantage of the correlation structure
across genomic loci in order to identify recurrent alterations more sensitively.
Additionally, we showed that Fisher's exact test can fail to control the false discovery rate
when there are many significant calls, under scenarios of
high signal-to-noise, high frequency, or large sample size.
This happens because investigators typically control
the FDR at the locus level while assuming independence 
(using the Benjamini-Hochberg method, for example).
Conversely, CNGPLD controls the FDR at the region level, as recommended by \cite{korthauer18}.

We also showed that CNGPLD maintains its sensitivity to detect differential 
CNA events at smaller sample sizes.
We note that CNGPLD becomes anti-conservative as
the sample size decreases
(whereas Fisher's exact test becomes excessively conservative),
likely because we assume that the summary scores are continuous
in our model, which is a better approximation with large sample sizes.
Because CNGPLD trades a moderate loss in precision for a substantial gain
in sensitivity at smaller sample sizes, we do not recommend users of our
tool to use more lenient FDR thresholds when analyzing smaller cohorts.
To mitigate the decrease in precision, users may impose additional filters on the called
events, based on such criteria such as the region size, 
the number of supporting markers,
and the magnitude of the latent difference.

We note here that CNGPLD identifies recurrent CNA regions that are 
differentially amplified or deleted in cases vs.\ controls at the population level.
Additional processing that identifies specific samples that harbour these recurrent
CNA events will depend on the platform and preprocessing pipeline used
in the study. In general, we recommend that users work with
copy-number estimates that are corrected for tumour purity and ploidy,
especially for identifying CNAs at the sample level, using appropriate
preprocessing and tools such as ABSOLUTE \citep{carter12}.

Additionally, we anticipate that future studies will begin to more frequently
incorporate case-control analyses of other genomic data.
Indeed, not only does our work provide a tool (CNGPLD) for doing comparative 
copy-number analyses, it also provides a statistical framework 
(Gaussian process latent difference) for comparisons of curves in general,
and this framework can be applied to 
other types of genomic or time series data,
given appropriate data preprocessing pipelines or 
extensions to the base model.

\section{Conclusion}

CNGPLD provides an approach to compare copy-number profiles between two cohorts and facilitates
the case-control comparison of cancer genome datasets in order to identify loci under differential
degrees of positive selection.
CNGPLD can be used to compare the copy-number profiles of different cancer cohorts 
in order to distinguish preferentially targeted drivers in cases vs.\ controls.

\section*{Acknowledgments}

We thank Jeffrey Miller and Keegan Korthauer for fruitful discussions.

\section*{Funding}

D.J.H.S.\ was supported by 
a Canadian Institutes of Health Research fellowship
and a fellowship from the Gulf Coast Consortia,
on the Computational Cancer Biology Training Program
(CPRIT Grant No.\ RP170593).
S.L.C.\ has been supported by awards from the Department of Defense (W81XWH-18-1-0357)
and NIH (R01CA227156).
This research has been supported in part by 
the Cancer Prevention and Research Institute of Texas grant RP170668 (W.J.Z.) and 
NIH/NCATS 1 UL1 TR003167 01 (W.J.Z.).
K.A.D.\ was partially supported by NCI Grant P30 CA016672, 
as well as NIH grants UL1TR003167 and 5R01GM122775.

\vspace*{5pt}

\noindent \textit{Conflict of Interest}: none declared.

\bibliographystyle{natbib}
\bibliography{cngpld}

\end{document}
